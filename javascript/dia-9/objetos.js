// Objetos en Javascript

var superman = {
    nombre: "Superman",
    "nombre real": "Clark kent",
    volar: function(){},
    laser: function(){},
}

console.log(superman["nombre real"]);
console.log(superman.nombre);
console.log(superman.volar());
console.log(superman["volar"]());

// Saber si existe una propiedad en un objeto
console.log("edad" in superman);
console.log(superman.edad !== undefined);

// Saber tosas las propiedad del Objeto
for(var key in superman){
    console.log(key + ":" + superman[key]);
}

//Añadir propiedades al objeto
superman.ciudad = "Metropolis";
console.log(superman);

//Eliminar una Propiedad del Objeto
delete superman.ciudad;

// Objeto Anidado o Conjunto de Objetos
justice_league = {
    superman: { realName: "Clark Kente"},
    batman: { realName: "Bruce Wayne"},
    wonderWoman: {realName: "Diana Prince"}
}

console.log(justice_league.superman.realName);


function saludo(options){
    var options = options || {};
    var saludo = options.saludo || "Hola";
    var nombre = options.nombre || "visitante";
    var edad = options.edad || 18;
    return saludo + "! Mi Nombre es: " + nombre + " y tengo" + edad + "de edad";
}
datos = {nombre: "Luis Javier", edad: 36 };
console.log(mensaje(datos)); 
console.log(mensaje({nombre: "CArlos, edad: 38"}));

// Javascript Object Notation or JSON parse convierte el string en objeto

var alumno = '{"nombre": "Pedro", "edad": 38, "Ciudad": "Caracas"}';
alumno = JSON.parse(alumno);
console.log(alumno.nombre);
console.log(alumno.edad);

var clark = JSON.stringify(superman); // hace lo contrario el objeto a string o cadena
console.log(clark);

// Objeto Math
console.log(Math.PI);
Console.log(Math.LN2);

var sueldo = 1320.65;
console.log(Math.ceil(sueldo));
console.log(Math.floor(sueldo));
console.log(Math.round(sueldo));

confirm.log(Math.random());
console.log(Math.floor(Math.random() * 5) + 1);

console.log(Math.abs(-1345.456));

console.log(Math.pow(3,2));


// Objeto Date();
hoy = new Date();
console.log(hoy);

// Devuelve el dia actual()
console.log(hoy.getUTCDay());

var dias = ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves","Viernes", "Sabado" ]; //arreglo
var meses = ["Enero", "Feb", "Mar", "Abr", "May", "Jun","Jul","Ago","Sep","Oct","Nov","Dic"];
var fecha = new Date();
console.log(dias[fecha.getDay()]);
console.log(meses[fecha.getMonth()]);
console.log(fecha.getHours());

// Expresiones Regulares : este ejemplo indica que la condicion son palabrass que contengan ing

var pattern_1 = /\w+ing/;
var pattern_2 = new RegExp('w+ing');

pattern = pattern.test("Cleaning");
console.log(prueba);

console.log(pattern_1.exec("Joke"));
console.log(pattern_1.exec("Joking"));

var soloVocales = /[aeiou]/;
console.log(soloVocales.test("aura"));

var alfabeto = /[A-Z]/;
var numeros = /[0-9]/;
var alfabeto = /[^A-Z]/ 

// \w = [A-Za-z0-9] coincide con cualquier caracter que sea un palabra
// \W = [A-Za-z0-9] coincide con cualquier caracter que no sea un palabra
// \d = [0-9] conincide con cualquier caracter que sea un digito
// \D = [^0-9] conincide con cualquier caracter que no sea un digito
// \s = [\t\r\n\f] coincide con cualquier caracter que  sea de espacio en blanco
// \S = [^\t\r\n\f] coincide con cualquier caracter que no sea de espacio en blanco

// Propiedades de las Expresiones Regulares

/[a-z]/g
/[a-z]/i
/[a-z]/m

























